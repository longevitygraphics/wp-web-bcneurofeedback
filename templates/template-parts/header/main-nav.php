<div class="main-navigation d-none d-xl-flex justify-content-between align-items-center px-3">
    <div class="header-social-media">
      <?php echo do_shortcode('[lg-social-media]'); ?>
    </div>
  	<nav class="navbar navbar-default">  
  	    <!-- Brand and toggle get grouped for better mobile display -->
  	    <div class="navbar-header d-none">
  	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
  	        <span class="sr-only">Toggle navigation</span>
  	        <span class="icon-bar"></span>
  	        <span class="icon-bar"></span>
  	        <span class="icon-bar"></span>
  	      </button>
  	    </div>


        <!-- Main Menu  -->
        <?php 

          $mainMenu = array(
            'menu'              => 'main-nav-left',
          	// 'theme_location'    => 'top-nav',
          	'depth'             => 2,
          	'container'         => 'div',
          	'container_class'   => 'collapse navbar-collapse',
          	'container_id'      => 'main-navbar',
          	'menu_class'        => 'nav navbar-nav',
          	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          	'walker'            => new WP_Bootstrap_Navwalker()
          );
          wp_nav_menu($mainMenu);

        ?>
        <div class="logo">
              <a href="/"><?php echo site_logo(); ?></a>
            </div>
         <?php
            $mainMenu = array(
                'menu'              => 'main-nav-right',
                // 'theme_location'    => 'top-nav',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'main-navbar',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker()
            );
            wp_nav_menu($mainMenu);

        ?>
    </nav>
    <div class="header-right">
      <?php
        $header_right_content = get_field('header_right_content', 'option');
        echo $header_right_content;
      ?>
    </div>
</div>