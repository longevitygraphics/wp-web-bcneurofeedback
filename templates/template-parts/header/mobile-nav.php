<div class="mobile-navigation main-navigation d-xl-none">
	<div class='d-flex justify-content-between align-items-center px-3'>
		<div class="logo">
			<a href="/"><?php echo site_logo(); ?></a>
		</div>
		<button class="mobile-toggle hamburger hamburger--collapse" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
			<div class="text-dark d-none d-lg-inline-block d-xl-none">Menu</div>
		</button>
	</div>
  	<nav class="navbar navbar-default">  
        <!-- Main Menu  -->
        <?php 

          $mobileMenu = array(
          	'menu'              => 'mobile-nav',
          	// 'theme_location'    => 'top-nav',
          	'depth'             => 2,
          	'container'         => 'div',
          	'container_class'   => 'collapse navbar-collapse',
          	'container_id'      => 'mobile-navbar',
          	'menu_class'        => 'nav navbar-nav',
          	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          	'walker'            => new WP_Bootstrap_Navwalker()
          );
          wp_nav_menu($mobileMenu);

        ?>
  	</nav>
</div>