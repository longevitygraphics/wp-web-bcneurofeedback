<div class='header-announcement d-flex justify-content-center align-items-center px-3'>
    <div>
        <svg data-ux="SVG" viewBox="0 0 14.57 25.34" dataaids="SEASONAL_SPRING_LEFT_ICON_RENDERED" class="x-el x-el-svg c1-1 c1-2 c1-1b c1-1c c1-1d c1-b c1-c c1-d c1-e c1-f c1-g x-d-ux">
            <g stroke="currentColor" fill="none" stroke-width="1" stroke-linejoin="round" stroke-dashoffset="100" stroke-dasharray="100">
                <path d="M5.59,25.25s-1-2.64,3.69-9.59A9.2,9.2,0,0,0,11,9.56,7.62,7.62,0,0,0,9.67,6,4.45,4.45,0,0,1,9,2.3a4,4,0,0,1,1.31-2,6,6,0,0,1,1,2.13A7,7,0,0,1,11.41,4a3.24,3.24,0,0,1-.65,1.69,2.63,2.63,0,0,1-.49.57s4.27,1.61,4-3.46c0,0-2.61.74-3.08,2" style="animation:draw 5s linear forwards;animation-delay:0.5s">

                </path>
                <g style="animation:draw 5s linear forwards;animation-delay:2.5s">
                    <path d="M6.55,20.62s-2.5-2.14-2.88-4.38a13.09,13.09,0,0,0-.8-2.86A4.36,4.36,0,0,0,.36,11.07s-.94,4.34,2.71,3.59">
                    </path>
                    <path d="M3.87,15.91s-1.05-4,1.07-4.84a3.59,3.59,0,0,1,.78,2.74A2.66,2.66,0,0,1,3.87,15.91Z"></path>
                </g>
            </g>
        </svg>
    </div>
    <?php do_action( "lg_announcement_bar") ?>
    <div>
        <svg data-ux="SVG" viewBox="0 0 18.76 25.34" dataaids="SEASONAL_SPRING_RIGHT_ICON_RENDERED" class="x-el x-el-svg c1-1 c1-2 c1-1b c1-1c c1-1d c1-b c1-c c1-d c1-e c1-f c1-g x-d-ux">
            <g stroke="currentColor" fill="none" stroke-width="1" stroke-linejoin="round" stroke-dashoffset="100" stroke-dasharray="100">
                <g style="animation:draw 5s linear forwards">
                    <path d="M8.91,2.38A7,7,0,0,0,8.76,4a3.24,3.24,0,0,0,.65,1.69,2.63,2.63,0,0,0,.49.57s-4.27,1.61-4-3.46c0,0,2.61.74,3.08,2"></path>
                    <path d="M5.59,25.25s-1-2.64,3.69-9.59A9.2,9.2,0,0,0,11,9.56,7.62,7.62,0,0,0,9.67,6,4.45,4.45,0,0,1,9,2.3a4,4,0,0,1,1.31-2,6,6,0,0,1,1,2.13A7,7,0,0,1,11.41,4a3.24,3.24,0,0,1-.65,1.69,2.63,2.63,0,0,1-.49.57s4.27,1.61,4-3.46c0,0-2.61.74-3.08,2"></path>
                </g>
                    <g style="animation:draw 5s linear forwards;animation-delay:2s">
                        <path d="M6.55,20.62s-2.5-2.14-2.88-4.38a13.09,13.09,0,0,0-.8-2.86A4.36,4.36,0,0,0,.36,11.07s-.94,4.34,2.71,3.59"></path>
                        <path d="M3.87,15.91s-1.05-4,1.07-4.84a3.59,3.59,0,0,1,.78,2.74A2.66,2.66,0,0,1,3.87,15.91Z"></path>
                    </g>
                    <g style="animation:draw 5s linear forwards;animation-delay:4s">
                        <path d="M14.23,12.86S17,9.78,15.46,8.06a3.59,3.59,0,0,0-1.93,2.09A2.66,2.66,0,0,0,14.23,12.86Z"></path>
                        <path d="M8.68,16.63s3.2-.78,4.55-2.61a13.09,13.09,0,0,1,2-2.19,4.36,4.36,0,0,1,3.28-.92s-1.12,4.29-4,2"></path>
                    </g>
                </g>
        </svg>
    </div>
</div>