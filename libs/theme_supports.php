<?php
function add_child_template_support()
{
  /*add theme colors*/
  add_theme_support('editor-color-palette', array(
    array(
      'name' => __('Primary', 'wp-theme-parent'),
      'slug' => 'primary',
      'color' => '#83B748'
    ),
    array(
      'name' => __('Secondary', 'wp-theme-parent'),
      'slug' => 'secondary',
      'color' => '#2E2E2E'
    ),
    array(
      'name' => __('Body Text', 'wp-theme-parent'),
      'slug' => 'body-text',
      'color' => '#333333'
    ),
    array(
      'name' => __('Light Gray', 'wp-theme-parent'),
      'slug' => 'light-gray',
      'color' => '#F8F8F8'
    ),
    array(
      'name' => __('FAQ Button', 'wp-theme-parent'),
      'slug' => 'faq-button',
      'color' => '#B5B5B5'
    ),
    array(
      'name' => __('White', 'wp-theme-parent'),
      'slug' => 'white',
      'color' => '#ffffff'
    ),
    array(
      'name' => __('Dark', 'wp-theme-parent'),
      'slug' => 'dark',
      'color' => '#333333'
    )
  ));

  //add support for editor styles
  add_theme_support("editor-styles");
}

add_action('after_setup_theme', 'add_child_template_support', 20);
